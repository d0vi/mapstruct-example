package com.gitlab.d0vi.mapstruct.example;

import lombok.Data;

@Data
public class CarDto {
    private String brand;
    private int seatCount;
    private String type;
}
