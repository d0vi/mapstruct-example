package com.gitlab.d0vi.mapstruct.example;

public enum CarType {
    SEDAN,
    COMPACT
}
