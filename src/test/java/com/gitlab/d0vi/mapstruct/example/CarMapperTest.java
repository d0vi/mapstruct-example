package com.gitlab.d0vi.mapstruct.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CarMapperTest {
    @Test
    public void shouldMapCarToDto() {
        Car car = new Car("Audi", 5, CarType.SEDAN);
        CarDto carDto = CarMapper.INSTANCE.carToCarDto(car);
        assertNotNull(carDto);
        assertEquals(carDto.getBrand(), "Audi");
        assertEquals(carDto.getSeatCount(), 5);
        assertEquals(carDto.getType(), "SEDAN");
    }
}
