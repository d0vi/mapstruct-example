package com.gitlab.d0vi.mapstruct.example;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Car {
    private String brand;
    private int numberOfSeats;
    private CarType type;
}
